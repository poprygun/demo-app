import { Controller, Get } from 'routing-controllers'
import { OpenAPI } from 'routing-controllers-openapi'

@Controller()
export default class IndexController {
	@Get('/')
	@OpenAPI({ summary: 'Default route' })
	index() {
		return 'OK'
	}
}
