import { Response } from 'express'
import { Photo } from '@interfaces/photos.interface'
import PhotosService from '@services/photos.service'
import { Body, Controller, Get, HttpCode, Post, QueryParams, Req, Res, UseBefore } from 'routing-controllers'
import authMiddleware from '@middlewares/auth.middleware'
import { OpenAPI } from 'routing-controllers-openapi'
import { RequestWithUser } from '@interfaces/auth.interface'
import { DeletePhotoDto, GetPhotosDto } from '@dtos/photos.dto'
import validationMiddleware from '@middlewares/validation.middleware'

@Controller()
export default class PhotosController {
	public photosService = new PhotosService()

	@Get('/get-photos')
	@OpenAPI({ summary: 'Returns list of photos with params' })
	@UseBefore(validationMiddleware(GetPhotosDto, 'query'))
	@HttpCode(200)
	async getPhotos(@Res() res: Response, @QueryParams() { ownerid, page, maxcount }: GetPhotosDto) {
		const photos: Photo[] = await this.photosService.findPhotos({
			ownerId: ownerid as string | undefined,
			page: parseInt(page as string) || 1,
			maxCount: parseInt(maxcount as string) || 10,
		})

		return res.status(200).json(photos)
	}

	@Get('/load-photos')
	@OpenAPI({ summary: 'Loads images to current user' })
	@UseBefore(authMiddleware)
	@HttpCode(200)
	async loadPhotos(@Req() req: RequestWithUser, @Res() res: Response) {
		const result = await this.photosService.loadPhoto(req.user)
		return res.status(201).json({ data: result })
	}

	@Post('/delete-photo')
	@OpenAPI({ summary: 'Delete photos by IDs one or more comma separated for current User' })
	@UseBefore(authMiddleware)
	@UseBefore(validationMiddleware(DeletePhotoDto, 'body'))
	@HttpCode(200)
	async deletePhotoByIds(@Body() { photoid }: DeletePhotoDto, @Req() req: RequestWithUser, @Res() res: Response) {
		const result = await this.photosService.deletePhotoByIds(photoid, req.user)
		return res.status(201).json(result)
	}
}
