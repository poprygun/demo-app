import { CreateUserDto, LoginUserDto, UserDto } from '@dtos/users.dto'
import { RequestWithUser } from '@interfaces/auth.interface'
import { User } from '@interfaces/users.interface'
import AuthService from '@services/auth.service'
import { Body, Controller, HttpCode, Post, Req, Res, UseBefore } from 'routing-controllers'
import { OpenAPI } from 'routing-controllers-openapi'
import authMiddleware from '@middlewares/auth.middleware'
import { Response } from 'express'
import ValidationMiddleware from '@middlewares/validation.middleware'

@Controller()
export default class AuthController {
	public authService = new AuthService()

	@Post('/register')
	@OpenAPI({ summary: 'Register new User by login, email and password' })
	@UseBefore(ValidationMiddleware(CreateUserDto, 'body'))
	@HttpCode(201)
	async register(@Body() userData: CreateUserDto, @Res() res: Response) {
		const result = <UserDto>await this.authService.register(userData)
		return res.json(result)
	}

	@Post('/login')
	@OpenAPI({ summary: 'Login existing User by login|email and password' })
	@UseBefore(ValidationMiddleware(LoginUserDto, 'body'))
	@HttpCode(200)
	async login(@Body() userData: LoginUserDto, @Res() res: Response) {
		const { cookie, findUser } = await this.authService.login(userData)
		return res.setHeader('Set-Cookie', [cookie]).json(findUser)
	}

	@Post('/logout')
	@OpenAPI({ summary: 'Logout logged in User' })
	@UseBefore(authMiddleware)
	@HttpCode(200)
	async logOut(@Req() req: RequestWithUser, @Res() res: Response) {
		const userData: User = req.user
		const logOutUserData: User = await this.authService.logout(userData)
		return res.setHeader('Set-Cookie', ['Authorization=; Max-age=0']).json(logOutUserData)
	}
}
