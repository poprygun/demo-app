import { User } from '@interfaces/users.interface'
import AlbumsService from '@services/albums.service'
import { AlbumChangeTitleDto, AlbumDeleteDto } from '@dtos/albums.dto'

import { Body, Controller, HttpCode, Post, Req, Res, UseBefore } from 'routing-controllers'
import { OpenAPI } from 'routing-controllers-openapi'
import authMiddleware from '@middlewares/auth.middleware'
import validationMiddleware from '@middlewares/validation.middleware'
import { Response } from 'express'

@Controller()
export default class AlbumsController {
	public albumsService = new AlbumsService()

	@Post('/change-album-title')
	@UseBefore(authMiddleware)
	@UseBefore(validationMiddleware(AlbumChangeTitleDto, 'body'))
	@OpenAPI({ summary: 'Change album title by albumid for current User' })
	@HttpCode(201)
	async changeAlbumTitle(
		@Req() req: Request & { user: User },
		@Body() albumData: AlbumChangeTitleDto,
		@Res() res: Response,
	) {
		const result = await this.albumsService.changeAlbumTitle({
			user: req.user,
			albumId: albumData.albumid,
			newAlbumName: albumData.new_album_name,
		})
		return res.json(result)
	}

	@Post('/delete-album')
	@UseBefore(authMiddleware)
	@UseBefore(validationMiddleware(AlbumDeleteDto, 'body'))
	@OpenAPI({ summary: 'Delete album by IDs for current User' })
	@HttpCode(201)
	async deleteAlbumByIds(
		@Req() req: Request & { user: User },
		@Body() albumData: AlbumDeleteDto,
		@Res() res: Response,
	) {
		const { albumid } = albumData
		const result = await this.albumsService.deleteAlbumByIds(albumid, req.user)
		return res.json(result)
	}
}
