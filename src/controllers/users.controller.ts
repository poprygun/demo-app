import { Response } from 'express'
import { CreateUserDto } from '@dtos/users.dto'
import { User } from '@interfaces/users.interface'
import UserService from '@services/users.service'
import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, Res, UseBefore } from 'routing-controllers'
import authMiddleware from '@middlewares/auth.middleware'
import { OpenAPI } from 'routing-controllers-openapi'

@Controller()
export default class UsersController {
	public userService = new UserService()

	@Get('/users')
	@UseBefore(authMiddleware)
	@OpenAPI({ summary: 'Return user list for authenticated User' })
	@HttpCode(200)
	async getUsers(@Res() res: Response) {
		const findAllUsersData: User[] = await this.userService.findAllUsers()
		return res.status(200).json(findAllUsersData)
	}

	@Get('/users/:id')
	@UseBefore(authMiddleware)
	@OpenAPI({ summary: 'Return user by id for authenticated User' })
	@HttpCode(200)
	async getUserById(@Res() res: Response, @Param('id') userId: string) {
		const findOneUserData: User = await this.userService.findUserById(userId)
		return res.status(200).json(findOneUserData)
	}

	@Post('/users')
	@UseBefore(authMiddleware)
	@OpenAPI({ summary: 'Creates new User for authenticated User' })
	@HttpCode(200)
	async createUser(@Body() userData: CreateUserDto, @Res() res: Response) {
		const createUserData: User = await this.userService.createUser(userData)
		return res.status(201).json(createUserData)
	}

	@Put('/users/:id')
	@UseBefore(authMiddleware)
	@OpenAPI({ summary: 'Updates User for authenticated User' })
	@HttpCode(200)
	async updateUser(@Body() userData: CreateUserDto, @Param('id') userId: string, @Res() res: Response) {
		const updateUserData: User = await this.userService.updateUser(userId, userData)
		return res.status(200).json(updateUserData)
	}

	@Delete('/users/:id')
	@UseBefore(authMiddleware)
	@OpenAPI({ summary: 'Deletes User for authenticated User' })
	@HttpCode(200)
	async deleteUser(@Param('id') userId: string, @Res() res: Response) {
		const deleteUserData: User = await this.userService.deleteUser(userId)
		return res.status(200).json(deleteUserData)
	}
}
