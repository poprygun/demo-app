import bcrypt from 'bcrypt'
import config from 'config'
import jwt from 'jsonwebtoken'
import { CreateUserDto, LoginUserDto, UserDto } from '@dtos/users.dto'
import { HttpException } from '@exceptions/HttpException'
import { DataStoredInToken, TokenData } from '@interfaces/auth.interface'
import { User } from '@interfaces/users.interface'
import userModel from '@models/users.model'
import { isEmpty } from '@utils/util'

class AuthService {
	public users = userModel

	public async register(userData: CreateUserDto): Promise<UserDto> {
		if (isEmpty(userData)) throw new HttpException(400, "You're not userData")

		const findUser: User = await this.users.findOne({ email: userData.email })
		if (findUser) throw new HttpException(409, `Your login or email ${userData.email} already exists`)

		const hashedPassword = await bcrypt.hash(userData.password, 10)
		return await this.users.create({ ...userData, password: hashedPassword })
	}

	public async login(userData: LoginUserDto): Promise<{ cookie: string; findUser: User }> {
		if (isEmpty(userData)) throw new HttpException(400, "You're not userData")
		const findUser: User = await this.users.findOne({
			$or: [{ login: userData.loginOrEmail }, { email: userData.loginOrEmail }],
		})
		if (!findUser) throw new HttpException(403, `Your login or email ${userData.loginOrEmail} not found`)

		const isPasswordMatching: boolean = await bcrypt.compare(userData.password, findUser.password)
		if (!isPasswordMatching) throw new HttpException(403, "You're password not matching")

		const tokenData = this.createToken(findUser)
		const cookie = this.createCookie(tokenData)
		return { cookie, findUser }
	}

	public async logout(userData: User): Promise<User> {
		if (isEmpty(userData)) throw new HttpException(400, "You're not userData")

		const findUser: User = await this.users.findOne({ email: userData.email, password: userData.password })
		if (!findUser) throw new HttpException(403, `Your login or email ${userData.email} not found`)
		delete findUser.password

		return findUser
	}

	public createToken(user: User): TokenData {
		const dataStoredInToken: DataStoredInToken = { _id: user._id }
		const secretKey: string = config.get('secretKey')
		const expiresIn: number = 60 * 60

		return { expiresIn, token: jwt.sign(dataStoredInToken, secretKey, { expiresIn }) }
	}

	public createCookie(tokenData: TokenData): string {
		return `Authorization=${tokenData.token}; HttpOnly; Max-Age=${tokenData.expiresIn};`
	}
}

export default AuthService
