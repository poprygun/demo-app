import axios from 'axios'
import { Photo } from '@interfaces/photos.interface'
import photoModel from '@models/photos.model'
import { User } from '@interfaces/users.interface'
import { Album } from '@interfaces/albums.interface'
import albumsModel from '@models/albums.model'
import { Document, FilterQuery } from 'mongoose'
import photosModel from '@models/photos.model'
import userModel from '@models/users.model'
import { isEmpty } from '@utils/util'
import { HttpException } from '@exceptions/HttpException'

interface Image {
	albumId: number
	id: number
	title: string
	url: string
	thumbnailUrl: string
}

export default class PhotoService {
	public photos = photoModel

	public async findPhotos({ ownerId = '', page = 1, maxCount = 10 }): Promise<Photo[]> {
		let filter: FilterQuery<Photo & Document> = {}

		if (!isEmpty(ownerId)) {
			const owner = await userModel.findById(ownerId)
			filter = { owner: owner }
		}

		const photos = this.photos
			.find(filter)
			.populate('albumId')
			.skip((page - 1) * maxCount)
			.limit(maxCount)

		return !isEmpty(ownerId) ? photos : photos.populate('owner', 'login email')
	}

	public async loadPhoto(user: User) {
		const images = <Image[]>(await axios.get('https://jsonplaceholder.typicode.com/photos')).data
		let photosCnt = 0
		let albumsCnt = 0

		await albumsModel.remove({ owner: user })
		await photosModel.remove({ owner: user })

		const photos: Photo[] = []
		const albumMap = {}
		for (const image of images) {
			let al = {} as Album
			if (albumMap[`Album #${image.albumId}`]) {
				al = albumMap[`Album #${image.albumId}`]
			} else {
				albumMap[`Album #${image.albumId}`] = await albumsModel.create({
					title: `Album #${image.albumId}`,
					owner: user,
				})
				albumsCnt++
				al = albumMap[`Album #${image.albumId}`]
			}
			photosCnt++
			photos.push({
				owner: user,
				albumId: al as Album,
				title: image.title,
				url: image.url,
				thumbnailUrl: image.thumbnailUrl,
			})
		}

		await photosModel.insertMany(photos)

		return { inserted: { albums: albumsCnt, photos: photosCnt } }
	}

	public async deletePhotoByIds(photoIds: string, owner: User): Promise<Photo[]> {
		const deletedPhoto = await this.photos.remove({ owner: owner, _id: { $in: photoIds.split(',') } })
		if (!deletedPhoto) throw new HttpException(409, 'Photo with this ID not found')

		return deletedPhoto
	}
}
