import { Album } from '@interfaces/albums.interface'
import albumsModel from '@models/albums.model'
import { Document } from 'mongoose'
import { User } from '@interfaces/users.interface'
import { HttpException } from '@exceptions/HttpException'
import photosModel from '@models/photos.model'

export default class AlbumsService {
	public albums = albumsModel
	public album: Album & Document
	public photos = photosModel

	public async changeAlbumTitle({ albumId, newAlbumName, user }): Promise<Album> {
		this.album = await this.albums.findOne({
			_id: albumId,
			owner: user,
		})
		if (!this.album) throw new Error(`Album with ID=${albumId} not found for user ${user.login}`)

		this.album.title = newAlbumName
		await this.album.save()

		return this.album
	}

	public async deleteAlbumByIds(albumIds: string, owner: User) {
		const albums = await this.albums.find({ owner: owner, _id: { $in: albumIds.split(',') } })
		const albumsIds = albums.map((i) => i._id)

		const deletedPhoto = await this.photos.remove({ owner: owner, albumId: { $in: albumsIds } })

		const deletedAlbum = await this.albums.remove({ owner: owner, _id: { $in: albumIds.split(',') } })
		if (!deletedAlbum) throw new HttpException(409, 'Album with this ID not found')

		return { albums: deletedAlbum, photos: deletedPhoto }
	}
}
