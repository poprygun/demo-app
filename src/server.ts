process.env['NODE_CONFIG_DIR'] = __dirname + '/configs'

import 'dotenv/config'
import validateEnv from '@utils/validateEnv'
import App from '@/app'
import IndexController from '@controllers/index.controller'
import AlbumsController from '@controllers/albums.controller'
import AuthController from '@controllers/auth.controller'
import PhotosController from '@controllers/photos.controller'
import UsersController from '@controllers/users.controller'

validateEnv()

const app = new App([IndexController, AuthController, AlbumsController, PhotosController, UsersController])

app.listen()
