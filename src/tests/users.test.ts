import bcrypt from 'bcrypt'
import mongoose from 'mongoose'
import request from 'supertest'
import App from '@/app'
import { CreateUserDto } from '@dtos/users.dto'
import UsersController from '@controllers/users.controller'
import { login } from '@/tests/auth.test'

let session = ''

describe('Testing Users', () => {
	beforeAll(async () => {
		session = await login()
	})

	afterAll(async () => {
		await new Promise<void>((resolve) => setTimeout(() => resolve(), 500))
	})

	describe('[GET] /users', () => {
		it('response fineAll Users', async () => {
			const usersController = new UsersController()
			const users = usersController.userService.users

			users.find = jest.fn().mockReturnValue([
				{
					_id: 'qpwoeiruty',
					login: 'a',
					email: 'a@email.com',
					password: await bcrypt.hash('q1w2e3r4!', 10),
				},
				{
					_id: 'alskdjfhg',
					login: 'b',
					email: 'b@email.com',
					password: await bcrypt.hash('a1s2d3f4!', 10),
				},
				{
					_id: 'zmxncbv',
					login: 'c',
					email: 'c@email.com',
					password: await bcrypt.hash('z1x2c3v4!', 10),
				},
			])
			;(mongoose as any).connect = jest.fn()
			const app = new App([UsersController])
			return request.agent(app.getServer()).get(`/users`).set('Cookie', session).expect(200)
		})
	})

	describe('[GET] /users/:id', () => {
		it('response findOne User', async () => {
			const userId = 'qpwoeiruty'

			const usersController = new UsersController()
			const users = usersController.userService.users

			users.findOne = jest.fn().mockReturnValue({
				_id: 'qpwoeiruty',
				login: 'a',
				email: 'a@email.com',
				password: await bcrypt.hash('q1w2e3r4!', 10),
			})
			;(mongoose as any).connect = jest.fn()
			const app = new App([UsersController])
			return request.agent(app.getServer()).get(`/users/${userId}`).set('Cookie', session).expect(200)
		})
	})

	describe('[POST] /users', () => {
		it('response Create User', async () => {
			const userData: CreateUserDto = {
				login: 'test2',
				email: 'test2@email.com',
				password: 'q1w2e3r42',
			}

			const usersController = new UsersController()
			const users = usersController.userService.users

			//users.findOne = jest.fn().mockReturnValue(null)
			users.create = jest.fn().mockReturnValue({
				_id: '60706478aad6c9ad19a31c88',
				login: userData.login,
				email: userData.email,
				password: await bcrypt.hash(userData.password, 10),
			})
			;(mongoose as any).connect = jest.fn()
			const app = new App([UsersController])
			return request.agent(app.getServer()).post(`/users`).send(userData).set('Cookie', session).expect(409)
		})
	})

	describe('[PUT] /users/:id', () => {
		it('response Update User', async () => {
			const userId = '60706478aad6c9ad19a31c84'
			const userData: CreateUserDto = {
				login: 'test',
				email: 'test@email.com',
				password: 'q1w2e3r4',
			}

			const usersController = new UsersController()
			const users = usersController.userService.users

			if (userData.email) {
				users.findOne = jest.fn().mockReturnValue({
					_id: userId,
					login: userData.login,
					email: userData.email,
					password: await bcrypt.hash(userData.password, 10),
				})
			}

			users.findByIdAndUpdate = jest.fn().mockReturnValue({
				_id: userId,
				login: userData.login,
				email: userData.email,
				password: await bcrypt.hash(userData.password, 10),
			})
			;(mongoose as any).connect = jest.fn()
			const app = new App([UsersController])
			return request.agent(app.getServer()).put(`/users/${userId}`).send(userData).set('Cookie', session).expect(200)
		})
	})

	describe('[DELETE] /users/:id', () => {
		it('response Delete User', async () => {
			const userId = '60706478aad6c9ad19a31c84'

			const usersController = new UsersController()
			const users = usersController.userService.users

			users.findByIdAndDelete = jest.fn().mockReturnValue({
				_id: '60706478aad6c9ad19a31c84',
				login: 'test',
				email: 'test@email.com',
				password: await bcrypt.hash('q1w2e3r4!', 10),
			})
			;(mongoose as any).connect = jest.fn()
			const app = new App([UsersController])
			return request.agent(app.getServer()).delete(`/users/${userId}`).set('Cookie', session).expect(200)
		})
	})
})
