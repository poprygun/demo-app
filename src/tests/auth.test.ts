import bcrypt from 'bcrypt'
import mongoose from 'mongoose'
import request from 'supertest'
import App from '@/app'
import { CreateUserDto, LoginUserDto } from '@dtos/users.dto'
import AuthController from '@controllers/auth.controller'

let session = ''

beforeAll(async () => {
	session = await login()
})

afterAll(async () => {
	await new Promise<void>((resolve) => setTimeout(() => resolve(), 500))
})

describe('Testing Auth', () => {
	describe('[POST] /register', () => {
		it('response should have the Create userData', async () => {
			const userData: CreateUserDto = {
				login: 'test',
				email: 'test@email.com',
				password: 'q1w2e3r4!',
			}

			const authController = new AuthController()
			const users = authController.authService.users

			users.findOne = jest.fn().mockReturnValue(null)
			users.create = jest.fn().mockReturnValue({
				_id: '60706478aad6c9ad19a31c84',
				login: userData.login,
				email: userData.email,
				password: await bcrypt.hash(userData.password, 10),
			})
			;(mongoose as any).connect = jest.fn()
			const app = new App([AuthController])
			return request(app.getServer()).post(`/register`).send(userData).expect(200)
		})
	})

	describe('[POST] /login', () => {
		it('response should have the Set-Cookie header with the Authorization token', async () => {
			const userData: CreateUserDto = {
				login: 'test',
				email: 'test@email.com',
				password: 'q1w2e3r4!',
			}

			const userLoginData: LoginUserDto = {
				loginOrEmail: userData.email,
				password: userData.password,
			}

			const authController = new AuthController()
			const users = authController.authService.users

			users.findOne = jest.fn().mockReturnValue({
				_id: '60706478aad6c9ad19a31c84',
				login: userData.login,
				email: userData.email,
				password: await bcrypt.hash(userData.password, 10),
			})
			;(mongoose as any).connect = jest.fn()
			const app = new App([AuthController])
			request
				.agent(app.getServer())
				.post(`/login`)
				.send(userLoginData)
				.expect('Set-Cookie', /^Authorization=.+/)
		})
	})

	describe('[POST] /logout', () => {
		it('logout Set-Cookie Authorization=; Max-age=0', async () => {
			const userData: CreateUserDto = {
				login: 'test',
				email: 'test@email.com',
				password: await bcrypt.hash('q1w2e3r4!', 10),
			}

			const authController = new AuthController()
			const users = authController.authService.users

			users.findOne = jest.fn().mockReturnValue(userData)
			;(mongoose as any).connect = jest.fn()
			const app = new App([AuthController])
			return request.agent(app.getServer()).post(`/logout`).set('Cookie', session).send(userData).expect(200)
		})
	})
})

export async function login() {
	const userData: CreateUserDto = {
		login: 'test',
		email: 'test@email.com',
		password: 'q1w2e3r4!',
	}

	const userLoginData: LoginUserDto = {
		loginOrEmail: userData.email,
		password: userData.password,
	}

	const authController = new AuthController()
	const users = authController.authService.users

	users.findOne = jest.fn().mockReturnValue({
		_id: '60706478aad6c9ad19a31c84',
		login: userData.login,
		email: userData.email,
		password: await bcrypt.hash(userData.password, 10),
	})
	;(mongoose as any).connect = jest.fn()
	const app = new App([AuthController])
	return request
		.agent(app.getServer())
		.post(`/login`)
		.send(userLoginData)
		.then((res) => {
			if (res.headers['set-cookie']) {
				return res.headers['set-cookie'][0]
					.split(',')
					.map((item) => item.split(';')[0])
					.join(';')
			}
		})
}
