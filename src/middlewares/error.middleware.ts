import { NextFunction, Request, Response } from 'express'
import { HttpException } from '@exceptions/HttpException'
import { logger } from '@utils/logger'

const errorMiddleware = (exception: HttpException, req: Request, res: Response, next: NextFunction) => {
	try {
		const status: number = exception.status || 500
		const error: string = exception.message || 'Something went wrong'

		logger.error(`[${req.method}] ${req.path} >> StatusCode:: ${status}, Message:: ${error}`)
		res.status(status).json({ error })
	} catch (error) {
		next(error)
	}
}

export default errorMiddleware
