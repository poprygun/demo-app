import { IsAlphanumeric, IsString, IsNumberString, IsOptional } from 'class-validator'

export class DeletePhotoDto {
	@IsString()
	public photoid: string
}

export class GetPhotosDto {
	@IsAlphanumeric()
	@IsOptional()
	public ownerid?: string

	@IsNumberString()
	@IsOptional()
	public page?: string

	@IsNumberString()
	@IsOptional()
	public maxcount?: string
}
