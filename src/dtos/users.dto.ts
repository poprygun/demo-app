import { IsAlphanumeric, IsEmail, IsString } from 'class-validator'

export class UserDto {
	@IsEmail()
	public email: string

	@IsAlphanumeric()
	public login: string
}

export class CreateUserDto {
	@IsEmail()
	public email: string

	@IsAlphanumeric()
	public login: string

	@IsString()
	public password: string
}

export class LoginUserDto {
	@IsString()
	public loginOrEmail: string

	@IsString()
	public password: string
}
