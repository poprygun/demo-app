import { IsAlphanumeric, IsString } from 'class-validator'

export class AlbumChangeTitleDto {
	@IsAlphanumeric()
	public albumid: string

	@IsString()
	public new_album_name: string
}

export class AlbumDeleteDto {
	@IsAlphanumeric()
	public albumid: string
}
