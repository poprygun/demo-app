/**
 * @swagger
 * components:
 *   schemas:
 *     Photo:
 *       type: object
 *       properties:
 *         _id:
 *           type: string
 *           description: The photo ID.
 *           example: 12345
 *         albumId:
 *           type: string
 *           description: Ref to album collection.
 *           example: 12345
 *         title:
 *           type: string
 *           description: Photo's title
 *         url:
 *           type: string
 *           description: Photo's url
 *         thumbnailUrl:
 *           type: string
 *           description: Photo's thumbnail url
 *         owner:
 *           type: string
 *           description: Ref to users collection.
 *           example: 12345
 */
import { User } from '@interfaces/users.interface'
import { Album } from '@interfaces/albums.interface'

export interface Photo {
	_id?: string
	albumId: Album
	title: string
	url: string
	thumbnailUrl: string
	owner: User
}
