/**
 * @swagger
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       properties:
 *         _id:
 *           type: string
 *           description: The user ID.
 *           example: 12345
 *         login:
 *           type: string
 *           description: The user's login.
 *           example: leanne
 *         email:
 *           type: string
 *           description: The user's email.
 *           example: leanne@graham.com
 *         registerDate:
 *           type: Date
 *           description: User creation timestamp
 *           example: 2021-12-01 23:01:56
 */

export interface User {
	_id: string
	login: string
	email: string
	password?: string
	registerDate: Date
}
