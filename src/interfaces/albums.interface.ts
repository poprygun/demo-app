/**
 * @swagger
 * components:
 *   schemas:
 *     Album:
 *       type: object
 *       properties:
 *         _id:
 *           type: string
 *           description: The album ID.
 *           example: 12345
 *         title:
 *           type: string
 *           description: The album's title
 *         owner:
 *           type: string
 *           description: Ref to users collection.
 *           example: 12345
 */
import { User } from '@interfaces/users.interface'

export interface Album {
	_id: string
	title: string
	owner: User
}
