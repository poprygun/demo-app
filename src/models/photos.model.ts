import mongoose, { model, Schema, Document } from 'mongoose'
import { Photo } from '@interfaces/photos.interface'

const photoSchema: Schema = new Schema({
	albumId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Album',
		required: true,
	},
	title: {
		type: String,
		required: true,
	},
	url: {
		type: String,
		required: true,
	},
	thumbnailUrl: {
		type: String,
		required: true,
	},
	owner: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	},
})

const photoModel = model<Photo & Document>('Photo', photoSchema)

export default photoModel
