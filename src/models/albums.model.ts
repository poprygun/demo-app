import mongoose, { model, Schema, Document } from 'mongoose'
import { Album } from '@interfaces/albums.interface'

const albumSchema: Schema = new Schema({
	title: {
		type: String,
		required: true,
	},
	owner: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	},
})

const albumModel = model<Album & Document>('Album', albumSchema)

export default albumModel
