import { model, Schema, Document } from 'mongoose'
import { User } from '@interfaces/users.interface'

const userSchema: Schema = new Schema({
	login: {
		type: String,
		required: true,
		unique: true,
	},
	email: {
		type: String,
		required: true,
		unique: true,
	},
	password: {
		type: String,
		required: true,
	},
	registerDate: {
		type: Date,
		default: Date.now,
		required: false,
	},
})

const userModel = model<User & Document>('User', userSchema)

export default userModel
