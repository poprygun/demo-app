# DemoApp

Demo app powered on NodeJS+Express+Mongoose

## Installation

Use the git to clone repo.

```bash
cd ~/projects
git clone https://gitlab.com/poprygun/demo-app.git
cd demo-app
```

## Usage

```bash
# Install dependencies
yarn install

# Set ENV
cp .env.example .env

# Development run
yarn dev

# Run test
yarn test

# Build production bundle
yarn build

# Production run (use dist/ directory)
yarn start
```

## Resources

- [Endpoints](http://localhost:3000)
- [Swagger](http://localhost:3000/api-docs)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
